﻿using RestaurantManagement.Domain.Common;
using RestaurantManagement.Domain.Item;
using RestaurantManagement.Domain.Table;
using RestaurantManagement.Domain.User;
using System.Collections.Generic;

namespace RestaurantManagement.Domain.Order
{
    public class OrderEntity : BaseEntity
    {
        private OrderEntity()
        {
        }

        private OrderEntity(int id, List<ItemEntity> itemEntities, UserEntity user, TableEntity table, OrderState orderState, decimal totalValue)
        {
            Id = id;
            ItemEntities = itemEntities;
            User = user;
            Table = table;
            OrderState = orderState;
            TotalValue = totalValue;
        }

        public static OrderEntity Create(int id, List<ItemEntity> itemEntities, UserEntity user, TableEntity table, OrderState orderState, decimal totalValue)
        {
            return new OrderEntity(id, itemEntities, user, table, orderState, totalValue);
        }

        public List<ItemEntity> ItemEntities { get; private set; }

        public UserEntity User { get; private set; }

        public TableEntity Table { get; private set; }

        public OrderState OrderState { get; private set; }

        public decimal TotalValue { get; private set; }
    }
}
