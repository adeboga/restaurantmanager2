﻿using RestaurantManagement.Domain.Item;

namespace RestaurantManagement.Application.Item.Command.UpdateItem.Repository
{
    public interface IItemRepositoryFacade
    {
        void UpdateItem(ItemEntity item);
    }
}
