﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Table.Command.ActivateTable
{
    public class ActivateTableModel
    {
        public int Id { get; set; }

        public int TableState { get; set; }

        public int NumberOfSeats { get; set; }

        public string User { get; set; }

    }
}
