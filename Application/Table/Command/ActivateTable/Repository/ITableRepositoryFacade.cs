﻿using RestaurantManagement.Domain.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Table.Command.ActivateTable.Repository
{
    public interface ITableRepositoryFacade
    {
        void UpdateTable(TableEntity table);

        void CreateOrder(TableEntity table);

        TableEntity GetTable(int id);
    }
}
