﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Domain.Order;
using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Table.Command.ActivateTable.Repository
{
    public class TableRepositoryFacade : ITableRepositoryFacade
    {
        private readonly ITableRepository _tableRepository;
        private readonly IOrderRepository _orderRepository;

        public TableRepositoryFacade(ITableRepository tableRepository)
        {
            _tableRepository = tableRepository;
        }

        public void CreateOrder(TableEntity table)
        {
            var order = OrderEntity.Create(0, null, null, table, OrderState.Created, 0);

            _orderRepository.Add(order);
        }

        public TableEntity GetTable(int id)
        {
            return _tableRepository
                .Get(id);
        }

        public void UpdateTable(TableEntity table)
        {
            table.Activate();
            _tableRepository.Update(table);
        }
    }
}
