﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Table.Command.ActivateTable
{
    public interface IActivateTableCommand
    {
        void Execute(ActivateTableModel model);
    }
}
