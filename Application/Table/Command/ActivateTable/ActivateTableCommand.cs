﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Application.Table.Command.ActivateTable.Factory;
using RestaurantManagement.Application.Table.Command.ActivateTable.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Table.Command.ActivateTable
{
    public class ActivateTableCommand : IActivateTableCommand
    {
        private readonly ITableRepositoryFacade _tableRepositoryFacade;
        private readonly ITableFactory _tableFactory;

        private readonly IUnitOfWork _unitOfWork;

       
        public void Execute(ActivateTableModel model)
        {
            var table = _tableRepositoryFacade.GetTable(model.Id);

            table.AssignTableToUser(model.User);

            _tableRepositoryFacade.CreateOrder(table);

            _tableRepositoryFacade.UpdateTable(table);

            _unitOfWork.Save();
        }
    }
}
