﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Table.Command.ActivateTable.Factory
{
    public class TableFactory : ITableFactory
    {
        public TableEntity Create(int id, int numberOfSeats, int tableState)
        {
           
          return TableEntity.Create(id, numberOfSeats, (TableState)tableState);
            
        }
    }
}
