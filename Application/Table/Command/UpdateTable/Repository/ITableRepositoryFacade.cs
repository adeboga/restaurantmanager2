﻿using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Table.Command.UpdateTable.Repository
{
    public interface ITableRepositoryFacade
    {
        void UpdateTable(TableEntity table);
    }
}
