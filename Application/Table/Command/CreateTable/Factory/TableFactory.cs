﻿using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Table.Command.CreateTable.Factory
{
    public class TableFactory
        : ITableFactory
    {
        public TableEntity Create(int numberOfSeats)
        {
            return TableEntity.Create(numberOfSeats);
        }
    }
}
