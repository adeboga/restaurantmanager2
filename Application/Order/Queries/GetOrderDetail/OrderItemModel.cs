﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Order.Queries.GetOrderDetail
{
    public class OrderItemModel
    {
       public int Id { get; set; }
        public string Name { get; set; }

        public decimal Price { get; set; }

    }
}
