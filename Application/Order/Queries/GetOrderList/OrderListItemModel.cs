﻿using RestaurantManagement.Domain.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Order.Queries.GetOrderList
{
    public class OrderListItemModel
    {
        public int Id { get; set; }
        public OrderState OrderState { get; set; }
        public decimal TotalValue { get; set; }
        public int TableID{ get; set; }
    }
}
