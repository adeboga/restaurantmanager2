﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Order.Queries.GetOrderList
{
    public interface IGetOrderListQuery
    {
        List<OrderListItemModel> Execute();
    }
}
