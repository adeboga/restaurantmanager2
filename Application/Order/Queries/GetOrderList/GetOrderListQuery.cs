﻿using RestaurantManagement.Application.Interfaces.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Order.Queries.GetOrderList
{
    public class GetOrderListQuery :IGetOrderListQuery
    {
        private readonly IOrderRepository _orderRepository;

        public GetOrderListQuery(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }
        public List<OrderListItemModel> Execute()
        {
            return _orderRepository
                .GetAll()
                .Select(order => new OrderListItemModel
                {
                    Id = order.Id,
                    OrderState = order.OrderState,
                    TableID = order.Table.Id,
                    TotalValue= order.TotalValue
                }).ToList();
        }
    }
}
