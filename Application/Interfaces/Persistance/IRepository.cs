﻿using System.Linq;

namespace RestaurantManagement.Application.Interfaces.Persistance
{
    public interface IRepository<T>
    {
        IQueryable<T> GetAll();

        T Get(int id);

        void Add(T entity);

        void Update(T entity);
    }
}
