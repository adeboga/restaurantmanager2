﻿using RestaurantManagement.Domain.Order;

namespace RestaurantManagement.Application.Interfaces.Persistance
{
    public interface IOrderRepository : IRepository<OrderEntity>
    {
    }
}
