﻿using RestaurantManagement.Application.Order.Queries.GetOrderDetail;
using RestaurantManagement.Application.Order.Queries.GetOrderList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RestaurantManagement.Presentation.Controllers
{

    [RoutePrefix("order")]
    public class OrderController : Controller
    {

        private readonly IGetOrderListQuery _getOrderListQuery;
        private readonly IGetOrderDetailQuery _getOrderDetailQuery;

        public OrderController(IGetOrderListQuery getOrderListQuery, IGetOrderDetailQuery getOrderDetailQuery)
        {
            _getOrderListQuery = getOrderListQuery;
            _getOrderDetailQuery = getOrderDetailQuery;

        }
        // GET: Order
        [Route("")]
        [HttpGet]
        [Authorize]
        public ActionResult Index()
        {
            var model = _getOrderListQuery.Execute();

            return View(model);
        }

        [Route("Details/{id}")]
        [HttpGet]
        [Authorize]
        // GET: Order/Details/5
        //public ActionResult Details(int id=0)
        public ActionResult Details(int id)
        {
            var model = _getOrderDetailQuery.Execute(id);
            return View(model);
        }

        // GET: Order/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Order/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Order/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Order/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Order/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Order/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
