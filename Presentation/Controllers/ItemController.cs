﻿using System;
using System.Web.Mvc;
using RestaurantManagement.Application.Item.Command.CreateItem;
using RestaurantManagement.Application.Item.Command.UpdateItem;
using RestaurantManagement.Application.Item.Queries.GetItemDetail;
using RestaurantManagement.Application.Item.Queries.GetItemList;
using RestaurantManagement.Domain.Common;
using RestaurantManagement.Presentation.Models.Item;

namespace RestaurantManagement.Presentation.Controllers
{
    [RoutePrefix("item")]
    public class ItemController : Controller
    {
        private readonly IGetItemListQuery _getItemListQuery;
        private readonly ICreateItemCommand _createCommand;
        private readonly IGetItemDetailQuery _getItemDetailQuery;
        private readonly IUpdateItemCommand _updateItemCommand;

        public ItemController(
            IGetItemListQuery getItemListQuery,
            ICreateItemCommand createCommand,
            IGetItemDetailQuery getItemDetailQuery,
            IUpdateItemCommand updateItemCommand)
        {
            _getItemListQuery = getItemListQuery;
            _createCommand = createCommand;
            _getItemDetailQuery = getItemDetailQuery;
            _updateItemCommand = updateItemCommand;
        }

        [Route("")]
        [HttpGet]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)]
        public ActionResult Index()
        {
            var model = _getItemListQuery.Execute();

            return View(model);
        }

        // GET: Item/Details/5
        [Route("details/{id}")]
        [HttpGet]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)]
        public ActionResult Details(int id)
        {
            var item = _getItemDetailQuery.Execute(id);

            var model = new ItemDetailViewModel
            {
                Item = item
            };

            return View(model);
        }

        [Route("create")]
        [HttpGet]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)]
        public ActionResult Create()
        {
            return View();
        }

        [Route("create")]
        [HttpPost]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)]
        public ActionResult Create(CreateItemViewModel viewModel)
        {
            try
            {
                var model = viewModel.Item;

                _createCommand.Execute(model);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Item/Edit/5
        [Route("edit/{id}")]
        [HttpGet]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)]
        public ActionResult Edit(int id)
        {
            var item = _getItemDetailQuery.Execute(id);

            var model = new UpdateItemViewModel
            {
                Item = new UpdateItemModel
                {
                    Id = item.Id,
                    Description = item.Description,
                    Name = item.Name,
                    Price = Convert.ToDecimal(item.Price)
                }
            };

            return View(model);
        }

        // POST: Item/Edit/5
        [HttpPost]
        [Route("edit")]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)]
        public ActionResult Edit(UpdateItemViewModel viewModel)
        {
            try
            {
                var model = viewModel.Item;

                _updateItemCommand.Execute(model);

                return RedirectToAction("index", "item");
            }
            catch
            {
                return View();
            }
        }

        // GET: Item/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Item/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
