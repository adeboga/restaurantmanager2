﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using Presentation.Models;
using RestaurantManagement.Domain.Common;

[assembly: OwinStartupAttribute(typeof(Presentation.Startup))]
namespace Presentation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRolesAndUsers();
        }

        private void CreateRolesAndUsers()
        {
            var context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (!roleManager.RoleExists(ApplicationRolesAvailable.Admin))
            {
                var role = new IdentityRole();
                role.Name = ApplicationRolesAvailable.Admin;
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "adeeboga@gmail.com";
                user.Email = "adeeboga@gmail.com";

                string userPassword = "Boga*12345";

                var checkUserCreation = userManager.Create(user, userPassword);

                if (checkUserCreation.Succeeded)
                {
                    userManager.AddToRole(user.Id, ApplicationRolesAvailable.Admin);
                }
            }

            if (!roleManager.RoleExists(ApplicationRolesAvailable.Waiter))
            {
                var role = new IdentityRole();
                role.Name = ApplicationRolesAvailable.Waiter;
                roleManager.Create(role);

                

                

            }
        }
    }
}
